var dataFromServer = [
    {
        valueFromServer: 1,
        nameFromServer: 'Пункт 1',
    },
    {
        valueFromServer: 2,
        nameFromServer: 'Пункт 2',
    },
    {
        valueFromServer: 3,
        nameFromServer: 'Пункт 3',
    },
    {
        valueFromServer: 4,
        nameFromServer: 'Пункт 4',
    },
    {
        valueFromServer: 5,
        nameFromServer: 'Пункт 5',
    },
    {
        valueFromServer: 6,
        nameFromServer: 'Пункт 6',
    },
    {
        valueFromServer: 555,
        nameFromServer: 'Пункт 555',
    },
    {
        valueFromServer: 100500,
        nameFromServer: 'Пункт 100500',
    },
];

var Mock = {
    loadOptions: function (url) {
        console.log(Utils.currentTime(), 'Запрашиваем данные с урла: ' + url);

        // Данные придут через 5 сек.
        return new Promise(function (resolve, reject) {
            setTimeout(function() {
                console.log(Utils.currentTime(), 'Данные получены.');
                resolve(JSON.stringify(dataFromServer));
            }, 5000);
        });
    },
};