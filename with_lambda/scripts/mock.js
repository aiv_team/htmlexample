/**
 * Этот файл - заглушка. Эмулятор сервера.
 */

var dataFromServer = [
    {
        valueFromServer: 1,
        nameFromServer: 'Пункт 1',
        children: [
            'Подпункт 1.1',
            'Подпункт 1.2',
            'Подпункт 1.3',
            'Подпункт 1.4',
        ],
    },
    {
        valueFromServer: 2,
        nameFromServer: 'Пункт 2',
        children: [
            'Подпункт 2.1',
            'Подпункт 2.2',
        ],
    },
    {
        valueFromServer: 3,
        nameFromServer: 'Пункт 3',
        children: [
            'Подпункт 3.1',
            'Подпункт 3.2',
            'Подпункт 3.3',
            'Подпункт 3.4',
            'Подпункт 3.5',
            'Подпункт 3.6',
        ],
    },
    {
        valueFromServer: 4,
        nameFromServer: 'Пункт 4',
        children: [
            'Подпункт 4.1',
        ],
    },
    {
        valueFromServer: 5,
        nameFromServer: 'Пункт 5',
        children: [
            'Подпункт 5.1',
            'Подпункт 5.2',
            'Подпункт 5.3',
        ],
    },
    {
        valueFromServer: 6,
        nameFromServer: 'Пункт 6',
        children: [
            'Подпункт 06.1',
            'Подпункт 06.2',
            'Подпункт 06.3',
            'Подпункт 06.4',
            'Подпункт 06.5',
        ],
    },
    {
        valueFromServer: 555,
        nameFromServer: 'Пункт 555',
        children: [
            'Подпункт 7.1',
            'Подпункт 7.2',
            'Подпункт 7.3',
            'Подпункт 7.4',
        ],
    },
    {
        valueFromServer: 100500,
        nameFromServer: 'Пункт 100500',
        children: [
            'Подпункт 100500.1',
            'Подпункт 100500.2',
            'Подпункт 100500.3',
            'Подпункт 100500.4',
        ],
    },
];

var Mock = {
    loadOptions: url => {
        console.log(Utils.currentTime(), 'Запрашиваем данные с урла: ' + url);

        // Данные придут через 5 сек.
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                console.log(Utils.currentTime(), 'Данные получены.');
                resolve(JSON.stringify(dataFromServer));
            }, 5000);
        });
    },
};