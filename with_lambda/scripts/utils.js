// Эти функи только для примера, как вынести функу в отдельный файл
var Utils = {
    leadingZero: val => val < 10 ? '0' + val : val,

    currentTime: () => {
        var now = new Date();
        var millis = now.getMilliseconds();
        millis = millis > 99 ? millis : '0' + (millis > 9 ? millis : '0' + millis);
        
        return Utils.leadingZero(now.getHours()) + ':' +
            Utils.leadingZero(now.getMinutes()) + ':' +
            Utils.leadingZero(now.getSeconds()) + '.' +
            millis + ' ';
    },
};